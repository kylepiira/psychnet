# TensorFlow
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.callbacks import ModelCheckpoint
# Scipy
import numpy as np
# General
from tqdm import tqdm
import json

# Settings
DATA_DIR = '.'
BATCH_SIZE = 64
EPOCHS = 100
VARIATIONS_PER_ROW = 50

with open(f'{DATA_DIR}/questions.json') as questions_file:
	questions = sorted([json.loads(q) for q in questions_file],
		key=lambda x: ''.join(list(filter(lambda z: z.isnumeric(), list(x.keys())[0]))))

with open(f'{DATA_DIR}/data.json') as data_file:
	data = [json.loads(a) for a in data_file]

train_x, train_y = [], []
for row in tqdm(data):
	for variation in range(VARIATIONS_PER_ROW):
		vector_x, vector_y = [], []
		for question in questions:
			success_p = np.random.random()
			if np.random.choice([True, False], p=[success_p, 1 - success_p]):
				vector_x.append(row[list(question.values())[0]])
			else:
				vector_x.append(0)
			vector_y.append(row[list(question.values())[0]])

		if not np.any(np.isnan(np.array(vector_y).astype(np.float32))):
			train_x.append(vector_x)
			train_y.append(vector_y)

train_x = np.array(train_x).astype(np.float32)
train_y = np.array(train_y).astype(np.float32)

model = Sequential()
model.add(Dense(128, activation='relu', input_shape=(len(questions),)))
model.add(Dropout(0.2))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(512, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(len(questions), activation='tanh'))

model.compile(loss='mse', optimizer='adam')

checkpoint = ModelCheckpoint(
	filepath='checkpoint.h5',
	save_best_only=True,
	monitor='val_loss',
)

model.fit(
	x=train_x,
	y=train_y,
	batch_size=BATCH_SIZE,
	epochs=EPOCHS,
	validation_split=0.1,
	callbacks=[checkpoint]
)

model.save('psychnet.h5')
